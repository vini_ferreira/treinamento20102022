<?php

use Adianti\Database\TRecord;

class Cidade extends TRecord
{
    const TABLENAME = 'cidades';
    const PRIMARYKEY= 'id';
    const IDPOLICY =  'serial'; // {max, serial}
    
    // use SystemChangeLogTrait;
    
    private $frontpage;
    private $unit;
    private $system_user_groups = array();
    private $system_user_programs = array();
    private $system_user_units = array();

    /**
     * Constructor method
     */
    public function __construct($id = NULL)
    {
        parent::__construct($id);
        parent::addAttribute('nome');
        parent::addAttribute('estado');
    }
}