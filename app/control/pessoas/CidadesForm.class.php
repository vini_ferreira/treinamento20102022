<?php

use Adianti\Control\TAction;
use Adianti\Control\TPage;
use Adianti\Validator\TRequiredValidator;
use Adianti\Widget\Container\TVBox;
use Adianti\Widget\Form\TEntry;
use Adianti\Widget\Form\TLabel;
use Adianti\Widget\Form\TRadioGroup;
use Adianti\Widget\Util\TXMLBreadCrumb;
use Adianti\Widget\Wrapper\TDBCombo;
use Adianti\Wrapper\BootstrapFormBuilder;

class CidadesForm extends TPage
{
    protected $form; // form

    // trait with onSave, onClear, onEdit
    use Adianti\Base\AdiantiStandardFormTrait;


    function __construct()
    {
        parent::__construct();

        $this->setDatabase('teste');    // defines the database
        $this->setActiveRecord('Cidade');   // defines the active record

        // creates the form
        $this->form = new BootstrapFormBuilder('form_Cidades');
        $this->form->setFormTitle("Cadastro de Cidade");
        $this->form->setClientValidation(true);

        // create the form fields
        $id       = new TEntry('id');
        $id->setEditable(FALSE);
        $nome     = new TEntry('nome');
        $estado     = new TRadioGroup('estado');
        $opcoesEstado = ["RS"=>"Rio Grande do Sul", "SC" =>"Santa Catarina"];
        $estado->addItems($opcoesEstado);

        // add the form fields
        $this->form->addFields([new TLabel('ID')], [$id]);
        $this->form->addFields([new TLabel('Nome', 'red')], [$nome]);
        $this->form->addFields([new TLabel('Estado', 'red')], [$estado]);

        $nome->addValidation('Nome', new TRequiredValidator);
        $estado->addValidation('Estado', new TRequiredValidator);

        // define the form action
        $this->form->addAction('Salvar', new TAction(array($this, 'onSave')), 'fa:save green');
        $this->form->addActionLink('Limpar',  new TAction(array($this, 'onClear')), 'fa:eraser red');
        $this->form->addActionLink('Listagem',  new TAction(array('CidadesList', 'onReload')), 'fa:table blue');
        // wrap the page content using vertical box
        $vbox = new TVBox;
        $vbox->style = 'width: 100%';
       // $vbox->add(new TXMLBreadCrumb('menu.xml', __CLASS__));
        $vbox->add($this->form);
        parent::add($vbox);
    }
}
